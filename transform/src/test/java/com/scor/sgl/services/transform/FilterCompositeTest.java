package com.scor.sgl.services.transform;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scor.sgl.services.transform.model.Mapping;


import javax.script.ScriptException;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class FilterCompositeTest {
    public static void main(String [] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode bigMap = new ObjectMapper().readValue(new File("/home/g0del/Desktop/tests/transform.json"), JsonNode.class);
        JsonNode filter = bigMap.get("mapping");
        List<Mapping> mappings = mapper.readValue(
                mapper.treeAsTokens(filter),
                new TypeReference<List<Mapping>>(){}
        );

        for(Mapping op: mappings) {
            System.out.print(op.getTarget_col());
          //  System.out.println(op.getOperation().evaluate());
        }
    }
}
