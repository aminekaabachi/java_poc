package com.scor.sgl.services.transform.service;

import com.scor.sgl.services.transform.dao.JobRepository;
import com.scor.sgl.services.transform.model.Job;
import com.scor.sgl.services.transform.model.Options;
import com.scor.sgl.services.transform.domain.TransformationThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Scope("prototype")
public class FilterTransformation {

    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    JobRepository repository;

    public Job executeAsynchronously(Options options) {
            Job filterJob =  new Job();
            filterJob.setOptions(options);
            repository.save(filterJob);

            TransformationThread job = applicationContext.getBean(TransformationThread.class, filterJob);
            taskExecutor.execute(job);

            return repository.findOne(filterJob.getId());
    }

    public List<Job> getAllJobs(String status) {
        List<Job> allJobs;
        if (status.equals("all"))
            allJobs = repository.findAll();
        else
            allJobs = repository.findByStatus(status);
        return allJobs;
    }

    public Job getJobDetails(long jobId) {
        return repository.findOne(jobId);
    }
}
