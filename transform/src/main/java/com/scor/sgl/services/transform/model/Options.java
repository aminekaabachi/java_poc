package com.scor.sgl.services.transform.model;

import java.io.Serializable;

public class Options implements Serializable {

    private Source source;

    private Sort[] sort;

    private Mapping[] mapping;

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public Sort[] getSort() {
        return sort;
    }

    public void setSort(Sort[] sort) {
        this.sort = sort;
    }

    public Mapping[] getMapping() {
        return mapping;
    }

    public void setMapping(Mapping[] mapping) {
        this.mapping = mapping;
    }
}
