package com.scor.sgl.services.transform.model;

import java.io.Serializable;

public class Sort implements Serializable {
    private String sort_col;
    private String order;

    public String getSort_col() {
        return sort_col;
    }

    public void setSort_col(String sort_col) {
        this.sort_col = sort_col;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }
}
