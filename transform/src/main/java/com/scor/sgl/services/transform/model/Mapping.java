package com.scor.sgl.services.transform.model;

import java.io.Serializable;

public class Mapping implements Serializable {
    private String target_col;
    private Operation operation;

    public String getTarget_col() {
        return target_col;
    }

    public void setTarget_col(String target_col) {
        this.target_col = target_col;
    }

    public Operation getOperation() {
        return operation;
    }

    public void setOperation(Operation operation) {
        this.operation = operation;
    }
}
