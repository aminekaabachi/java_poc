package com.scor.sgl.services.transform.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scor.sgl.services.transform.dao.JobRepository;
import com.scor.sgl.services.transform.model.Job;
import com.scor.sgl.services.transform.model.Options;
import com.scor.sgl.services.transform.model.Source;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import org.jetbrains.annotations.NotNull;
import org.mapdb.Serializer;
import org.mapdb.SortedTableMap;
import org.mapdb.volume.MappedFileVol;
import org.mapdb.volume.Volume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
@Scope("prototype")
public class TransformationThread implements Runnable {

    private Job job;

    @Autowired
    JobRepository repository;

    private final List<Thread> workers;
    private SyncWriter sw;

    public TransformationThread(Job job) {
        this.job = job;
        this.workers = new ArrayList<>();
    }

    @Override
    public void run() {

        long start = System.currentTimeMillis();

        updateJobStatusRunning();

        CsvParser parser = initReader();

        Options options = job.getOptions();
        parser.beginParsing(getReader(options.getSource().getPath()));
        //File metadata =  new File(options.getSource().getMetadata());
        HashMap<String,String> columnsTypes = null;
        HashMap<String,String> outputMetadata = new HashMap<>();
        List<String> header = Arrays.asList(parser.parseNext());

        File file =new File(options.getSource().getPath());
        String output = System.getProperty("user.home")+"/dummy/output/transform"+start+".csv";
        String outputMeta = System.getProperty("user.home")+"/dummy/output/transform"+start+".json";
        String db = System.getProperty("user.home")+"/dummy/output/transform"+start+".csv";


        Volume volume = MappedFileVol.FACTORY.makeVolume(db, false);
        SortedTableMap.Sink<Integer,String> sink =
                SortedTableMap.create(
                        volume,
                        Serializer.INTEGER,
                        Serializer.STRING
                ).createFromSink();

        for(String key: header) {
            outputMetadata.put(key, "string");
        }

        try {
            (new ObjectMapper()).writeValue(new File(outputMeta),outputMetadata);
        } catch (IOException e) {
            e.printStackTrace();
        }

        sw = new SyncWriter(new File(output), header);
        List<String[]> data = startWorkers(parser, options, columnsTypes, header, sink);
        handlingEndData(options, columnsTypes, header, sink, data, data.size() > 0);
        waitForWorkers();


        SortedTableMap<Integer, String> map = sink.create();
        for (Object r: map.values())
            sw.write((String[]) r);
        sw.close();


        updateJobStatusDone(start, output, outputMeta);

    }

    private void updateJobStatusRunning() {
        job.setStatus("running");
        repository.save(job);
    }

    private void updateJobStatusDone(long start, String output, String outputMeta) {
        job.setStatus("done");
        Source outputSource = new Source();
        outputSource.setType("file");
        outputSource.setPath(output);
        outputSource.setMetadata(outputMeta);
        job.setOutput(outputSource);
        job.setTime((int) TimeUnit.MILLISECONDS.toSeconds((int) (System.currentTimeMillis() - start)));
        repository.save(job);
    }

    @NotNull
    private CsvParser initReader() {
        CsvParserSettings settings = new CsvParserSettings();
        settings.getFormat().setDelimiter(';');
        settings.getFormat().setLineSeparator("\n");
        return new CsvParser(settings);
    }

    private void handlingEndData(Options options, HashMap<String, String> columnsTypes, List<String> header, SortedTableMap.Sink<Integer, String> sink, List<String[]> data, boolean b) {
        if (b) {
            Thread worker = new Thread(new ProcessChunkThread(options, header, header, columnsTypes, data, sink));
            worker.start();
            workers.add(worker);
            data.clear();
        }
    }

    @NotNull
    private List<String[]> startWorkers(CsvParser parser, Options options, HashMap<String, String> columnsTypes, List<String> header, SortedTableMap.Sink<Integer, String> sink) {
        List<String[]> data = new ArrayList<>();
        String[] row = null;
        int i = 0;
        while ((row = parser.parseNext()) != null) {
            handlingEndData(options, columnsTypes, header, sink, data, data.size() >= 10000);

            if (i > 0 && i % 400000 == 0) {
                waitForWorkers();
            }

            data.add(row);
            i++;
        }
        return data;
    }

    private void waitForWorkers() {
        for (Thread worker : workers) {
            try {
                worker.join();
            } catch (InterruptedException e) {
                //e.printStackTrace();
            }
        }
        workers.clear();
    }

    Reader getReader(String relativePath) {
        try {
            return new InputStreamReader(new FileInputStream(new File(relativePath)), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            //e.printStackTrace();
        } catch (FileNotFoundException e) {
            //e.printStackTrace();
        }
        return null;
    }

}