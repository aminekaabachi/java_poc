package com.scor.sgl.services.transform.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class Operation implements Serializable {

    private String op;
    private Operation left;
    private Operation middle;
    private Operation right;

    public Operation() {
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public Operation getLeft() {
        return left;
    }

    public void setLeft(Operation left) {
        this.left = left;
    }

    public Operation getMiddle() {
        return middle;
    }

    public void setMiddle(Operation middle) {
        this.middle = middle;
    }

    public Operation getRight() {
        return right;
    }

    public void setRight(Operation right) {
        this.right = right;
    }

    public Operation(String op, Operation left, Operation middle, Operation right) {
        super();
        this.op = op;
        this.left = left;
        this.middle = middle;
        this.right = right;
    }

    public String evaluate(ConcurrentHashMap<String, String> rowValues) {
        try {
            List<String> p_op_p = Arrays.asList("+", "-", "*", "/", "<", ">", "<=", ">=", "or", "and", "=");
            List<String> op_p_p_p = Arrays.asList("round", "power", "instr", "substring", "datefromparts", "replace", "abs"
                    , "addate", "datediff", "year", "month", "day", "trim", "floor", "min", "max", "length", "avg");
//		"equal","not equal","not","instrrev","percentage","median","mode","range"
            if (this.op == null) {
                return "";
            } else if (this.left == null & this.middle == null & this.right == null) {
                return this.op;
            } else if (p_op_p.contains(op)) {
                switch (op) {
                    case "+":
                        rowValues.put(this.getLeft().toString(),this.getLeft().evaluate(rowValues).concat(this.getRight().evaluate(rowValues)));
                    default:
                        rowValues.put(this.getLeft().toString(), this.getRight().evaluate(rowValues));
                }
                //return "(" + this.getLeft().evaluate() + op + this.getRight().evaluate() + ")";
            } else if (op_p_p_p.contains(op)) {
                List<String> params = Arrays.asList(this.left.evaluate(rowValues), this.middle.evaluate(rowValues), this.right.evaluate(rowValues));
                params.removeIf(""::equals);
                rowValues.put(this.getLeft().toString(), op + "(" + String.join(", ", params) + ")");
            } else if (op.equals("if_then_else")) {
                //return "CASE WHEN "+left.evaluate()+" THEN "+middle.evaluate()+" ELSE "+right.evaluate() + " END";
                if (Boolean.valueOf(left.evaluate(rowValues))) {
                    return rowValues.put(this.getLeft().toString(), middle.evaluate(rowValues));
                } else {
                    return rowValues.put(this.getLeft().toString(), right.evaluate(rowValues));
                }
            }
            return "### untreated case ###";
        } catch (Exception e) {
            return "";
        }
    }
}