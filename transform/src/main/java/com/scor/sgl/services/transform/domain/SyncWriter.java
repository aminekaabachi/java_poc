package com.scor.sgl.services.transform.domain;


import com.univocity.parsers.csv.CsvWriter;
import com.univocity.parsers.csv.CsvWriterSettings;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

class SyncWriter {
    CsvWriter writer;


    public SyncWriter(File file, List<String> headers) {
        CsvWriterSettings settings = new CsvWriterSettings();
        settings.getFormat().setDelimiter(';');
        writer = new CsvWriter(file, settings);
        writer.writeHeaders(headers);
    }

    public void close() {
        writer.close();
    }

    public synchronized void write(String[] row) {
        writer.writeRow(row);
    }

    public synchronized void writeAll(ArrayList<Object[]> results) {
        writer.writeRows(results);
    }
}

