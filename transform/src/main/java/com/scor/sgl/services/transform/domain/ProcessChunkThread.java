package com.scor.sgl.services.transform.domain;

import com.scor.sgl.services.transform.model.Mapping;
import com.scor.sgl.services.transform.model.Options;
import org.mapdb.SortedTableMap;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

class ProcessChunkThread implements Runnable {

    private final Options options;
    private final List<String> header;
    private final  List<String> outputHeaders;
    private final HashMap<String, String> columnsTypes;
    private volatile List<String[]> data;
    private final SortedTableMap.Sink<Integer, String> sink;
    private volatile int k = 0;

    public ProcessChunkThread(Options options, List<String> header, List<String> outputHeaders, HashMap<String, String> columnsTypes, List<String[]> data, SortedTableMap.Sink<Integer, String> sink) {
        this.options = options;
        this.header = header;
        this.outputHeaders = outputHeaders;
        this.columnsTypes = columnsTypes;
        this.data = new ArrayList<>(data);
        this.sink = sink;
    }

    public String type(String column) {
        return columnsTypes.getOrDefault(column, "string");
    }

    @Override
    public void run() {
        ArrayList<Object[]> results = new ArrayList<>();

        System.out.println(data.size());
        ConcurrentHashMap<String, String> rowValues = new ConcurrentHashMap<>();

        for (String[] row : data) {
            String[] returnedRow = new String[outputHeaders.size()];

            for (int i = 0; i<header.size(); i++) {
                if (row[i] != null) {
                    rowValues.put(header.get(i), row[i]);
                }
            }
                for (Mapping m:  this.options.getMapping())
                        m.getOperation().evaluate(rowValues);

            for (int i = 0; i<header.size(); i++) {
                returnedRow[i] = rowValues.get(header.get(i));
            }

            results.add(returnedRow);
            rowValues.clear();
        }
        System.out.println("done "+ data.size());


        for (Object[] r: results)
            try {
                sink.put(Integer.parseInt((String) r[0]), r.toString());
            } catch(Exception e) {
                sink.put(0, r.toString());
            }
        //
        data.clear();
    }
}
