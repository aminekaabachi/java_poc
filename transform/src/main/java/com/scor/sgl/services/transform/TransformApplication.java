package com.scor.sgl.services.transform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;


@SpringBootApplication
@EnableDiscoveryClient
@EnableAsync
@EnableJpaRepositories
public class TransformApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransformApplication.class, args);
	}

}
