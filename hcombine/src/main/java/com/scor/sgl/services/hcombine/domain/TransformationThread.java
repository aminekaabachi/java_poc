package com.scor.sgl.services.hcombine.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scor.sgl.services.hcombine.dao.JobRepository;
import com.scor.sgl.services.hcombine.model.Job;
import com.scor.sgl.services.hcombine.model.Options;
import com.scor.sgl.services.hcombine.model.Source;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@Scope("prototype")
public class TransformationThread implements Runnable {

    Job job;

    @Autowired
    JobRepository repository;

    private final List<Thread> workers;
    private SyncWriter sw;

    public TransformationThread(Job job) {
        this.job = job;
        this.workers = new ArrayList<>();
    }

    @Override
    public void run() {

        long start = System.currentTimeMillis();

        job.setStatus("running");
        repository.save(job);

        CsvParserSettings settings = new CsvParserSettings();
        settings.getFormat().setDelimiter(';');
        settings.getFormat().setLineSeparator("\n");

        int k = 0;
        CsvParser leftparser = new CsvParser(settings);
        CsvParser rightparser = new CsvParser(settings);

        Options options = job.getOptions();
        String left = options.getSource()[0].getPath();
        String right = options.getSource()[1].getPath();

        leftparser.beginParsing(getReader(left));
        rightparser.beginParsing(getReader(right));

        HashMap<String,String> outputMetadata1 = new HashMap<>();
        HashMap<String,String> outputMetadata2 = new HashMap<>();

        List<String> header = new ArrayList<>();
        List<String> leftheader = Arrays.asList(leftparser.parseNext());
        List<String> rightheader = Arrays.asList(rightparser.parseNext());

        for (int p=0; p<leftheader.size(); p++)
            leftheader.set(p, "left."+leftheader.get(p));

        header.addAll(leftheader);
        header.addAll(rightheader);

        Map<String, Integer> columnsIndexes = new HashMap<>();

        setupJoinKeys(options, outputMetadata1, outputMetadata2, header, columnsIndexes);

        File file =new File(options.getSource()[0].getPath());

        double bytes = file.length();
        double kilobytes = (bytes / 1024);
        double megabytes = (kilobytes / 1024);
        double gigabytes = (megabytes / 1024);

        String path = System.getProperty("user.home")+"/dummy/hcombine"+start+".db";
        String output = System.getProperty("user.home")+"/dummy/output/hcombine"+start+".csv";

        String outputMeta = System.getProperty("user.home")+"/dummy/output/hcombine"+start+".json";

        Map<String, String> outputMetadata = new HashMap<>();
        outputMetadata.putAll(outputMetadata1);
        outputMetadata.putAll(outputMetadata2);

        DB db = null;
        if (gigabytes < 5) {
            db = DBMaker.memoryDB().make();
        } else {
            db = DBMaker
                    .fileDB(path)
                    .fileMmapEnable()
                    .closeOnJvmShutdown()
                    .make();
        }
        ConcurrentMap<String, String[]> map = (ConcurrentMap) db.hashMap("mapped").create();
        String[] row = null;
        String key = "";

        CsvParser parser = null;
        CsvParser otherparser = null;
        int padding = 0;
        if (options.getJoins()[1].getType().equals("RIGHT JOIN")) {
            parser = rightparser;
            otherparser = leftparser;
            padding=leftheader.size();
        }
        else {
            parser = leftparser;
            otherparser = rightparser;
        }

        createJoinMap(options, columnsIndexes, map, key, parser, padding, 1);

        List<String> outputHeaders = new ArrayList<>();

        outputHeaders.addAll(leftheader.stream().filter(h-> outputMetadata1.containsKey(h)).map(m->m.replace("left.","")).collect(Collectors.toList()));
        outputHeaders.addAll(rightheader.stream().filter(h-> outputMetadata2.containsKey(h)).collect(Collectors.toList()));

        sw = new SyncWriter(new File(output), outputHeaders);

        Thread worker = new Thread(new ProcessChunkThread(0, sw, otherparser, leftheader.size(), outputMetadata1.size(), options, header, columnsIndexes, outputHeaders, map));
        worker.start();
        try {
            worker.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        HashMap<String, String> outputMetadata3 = new HashMap<>();
        for (String kk: outputMetadata.keySet()) {
            if (kk.contains("left.")) {
                outputMetadata3.put(kk.replace("left.", ""), outputMetadata.get(kk));
            } else {
                outputMetadata3.put(kk, outputMetadata.get(kk));
            }
        }

        try {
            (new ObjectMapper()).writeValue(new File(outputMeta),outputMetadata3);
        } catch (IOException e) {
            e.printStackTrace();
        }

        sw.close();

        for (int p=1; p<options.getJoins().length-1; p++)  {
            left = output;
            right = options.getSource()[p+1].getPath();

            leftparser.beginParsing(getReader(left));
            rightparser.beginParsing(getReader(right));

            outputMetadata1.clear();
            outputMetadata2.clear();

            header.clear();
            leftheader = Arrays.asList(leftparser.parseNext());
            rightheader = Arrays.asList(rightparser.parseNext());

            for (int l=0; l<leftheader.size(); l++)
                leftheader.set(l, "left."+leftheader.get(l));

            header.addAll(leftheader);
            header.addAll(rightheader);

            columnsIndexes.clear();

            fixingJoinKeys(options, outputMetadata1, outputMetadata2, header, columnsIndexes, outputMetadata, p);

            file = new File(output);

            bytes = file.length();
            kilobytes = (bytes / 1024);
            megabytes = (kilobytes / 1024);
            gigabytes = (megabytes / 1024);

            long start2 = System.currentTimeMillis();

            path = System.getProperty("user.home")+"/dummy/hcombine"+start2+".db";
            output = System.getProperty("user.home")+"/dummy/output/hcombine"+start2+".csv";

            outputMeta = System.getProperty("user.home")+"/dummy/output/hcombine"+start2+".json";

            outputMetadata.clear();
            outputMetadata.putAll(outputMetadata1);
            outputMetadata.putAll(outputMetadata2);



            db = null;
            if (gigabytes < 5) {
                db = DBMaker.memoryDB().make();
            } else {
                db = DBMaker
                        .fileDB(path)
                        .fileMmapEnable()
                        .closeOnJvmShutdown()
                        .make();
            }
            map = (ConcurrentMap) db.hashMap(String.valueOf(start2)).create();
            row = null;
            key = "";

            parser = null;
            otherparser = null;
            padding = 0;
            if (options.getJoins()[p+1].getType().equals("RIGHT JOIN")) {
                parser = rightparser;
                otherparser = leftparser;
                padding=leftheader.size();
            }
            else {
                parser = leftparser;
                otherparser = rightparser;
            }

            createJoinMap(options, columnsIndexes, map, key, parser, padding, p+1);

            outputHeaders.clear();

            outputHeaders.addAll(leftheader.stream().map(m->m.replace("left.","")).collect(Collectors.toList()));
            outputHeaders.addAll(rightheader.stream().filter(h-> outputMetadata2.containsKey(h)).collect(Collectors.toList()));

            sw = new SyncWriter(new File(output), outputHeaders);

            startWorkers(options, outputMetadata1, header, leftheader, columnsIndexes, map, otherparser, outputHeaders, p);

            outputMetadata3.clear();
            for (String kk: outputMetadata.keySet()) {
                if (kk.contains("left.")) {
                    outputMetadata3.put(kk.replace("left.", ""), outputMetadata.get(kk));
                } else {
                    outputMetadata3.put(kk, outputMetadata.get(kk));
                }
            }

            try {
                (new ObjectMapper()).writeValue(new File(outputMeta),outputMetadata3);
            } catch (IOException e) {
                e.printStackTrace();
            }

            sw.close();
        }

        map.clear();
        db.close();
        updateJobStatusDone(start, output, outputMeta);

    }

    private void setupJoinKeys(Options options, HashMap<String, String> outputMetadata1, HashMap<String, String> outputMetadata2, List<String> header, Map<String, Integer> columnsIndexes) {
        for(String key: options.getJoins()[1].getKey_cols().values()) {
            columnsIndexes.put("left."+key, header.indexOf("left."+key));
        }

        for(String key: options.getJoins()[1].getKey_cols().keySet()) {
            columnsIndexes.put(key, header.indexOf(key));
        }


        for(String key: options.getJoins()[0].getSelection_cols()) {
            outputMetadata1.put("left."+key, "string");
        }

        for(String key: options.getJoins()[1].getSelection_cols()) {
            outputMetadata2.put(key, "string");
        }

        if (options.getJoins()[1].getType().equals("LEFT JOIN")) {
            options.getJoins()[1].setType("RIGHT JOIN");
        }else if (options.getJoins()[1].getType().equals("RIGHT JOIN")) {
            options.getJoins()[1].setType("LEFT JOIN");
        }
    }

    private void fixingJoinKeys(Options options, HashMap<String, String> outputMetadata1, HashMap<String, String> outputMetadata2, List<String> header, Map<String, Integer> columnsIndexes, Map<String, String> outputMetadata, int p) {
        for(String key2: options.getJoins()[p+1].getKey_cols().values()) {
            columnsIndexes.put("left."+key2, header.indexOf("left."+key2));
        }

        for(String key2: options.getJoins()[p+1].getKey_cols().keySet()) {
            columnsIndexes.put(key2, header.indexOf(key2));
        }


        for(String key2: outputMetadata.keySet()) {
            outputMetadata1.put(key2, outputMetadata.get(key2));
        }

        for(String key2: options.getJoins()[p+1].getSelection_cols()) {
            outputMetadata2.put(key2, "string");
        }

        if (options.getJoins()[p+1].getType().equals("LEFT JOIN")) {
            options.getJoins()[p+1].setType("RIGHT JOIN");
        }else if (options.getJoins()[p+1].getType().equals("RIGHT JOIN")) {
            options.getJoins()[p+1].setType("LEFT JOIN");
        }
    }

    private void updateJobStatusDone(long start, String output, String outputMeta) {
        job.setStatus("done");
        Source outputSource = new Source();
        outputSource.setType("file");
        outputSource.setPath(output);
        outputSource.setMetadata(outputMeta);
        job.setOutput(outputSource);
        job.setTime((int) TimeUnit.MILLISECONDS.toSeconds((int) (System.currentTimeMillis() - start)));
        repository.save(job);
    }

    private void startWorkers(Options options, HashMap<String, String> outputMetadata1, List<String> header, List<String> leftheader, Map<String, Integer> columnsIndexes, ConcurrentMap<String, String[]> map, CsvParser otherparser, List<String> outputHeaders, int p) {
        Thread worker;
        worker = new Thread(new ProcessChunkThread(p, sw, otherparser, leftheader.size(), outputMetadata1.size(), options, header, columnsIndexes, outputHeaders, map));
        worker.start();
        try {
            worker.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void createJoinMap(Options options, Map<String, Integer> columnsIndexes, ConcurrentMap<String, String[]> map, String key, CsvParser parser, int padding, int i) {
        String[] row;
        while ((row = parser.parseNext()) != null) {
            List<String> keys = null;
            if (options.getJoins()[i].getType().equals("RIGHT JOIN"))
                keys = new ArrayList<>(options.getJoins()[i].getKey_cols().keySet());
            else {
                keys = new ArrayList<>(options.getJoins()[i].getKey_cols().values());
                for (int p = 0; p < keys.size(); p++)
                    keys.set(p, "left." + keys.get(p));
            }

            for (String k2 : keys) {
                try {
                    key += row[columnsIndexes.get(k2) - padding].trim().toLowerCase();
                } catch (Exception e) {

                }
            }
            map.put(key, row);
            key = "";
        }
    }

    public Reader getReader(String relativePath) {
        try {
            return new InputStreamReader(new FileInputStream(new File(relativePath)), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}