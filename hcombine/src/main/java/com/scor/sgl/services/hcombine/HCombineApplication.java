package com.scor.sgl.services.hcombine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableDiscoveryClient
@EnableAsync
@EnableJpaRepositories
public class HCombineApplication {

	public static void main(String[] args) {
		SpringApplication.run(HCombineApplication.class, args);
	}

}
