package com.scor.sgl.services.hcombine.model;

import java.io.Serializable;
import java.util.HashMap;

public class Join implements Serializable {
    private String type;
    private String[] selection_cols;

    private HashMap<String, String> key_cols;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String[] getSelection_cols() {
        return selection_cols;
    }

    public void setSelection_cols(String[] selection_cols) {
        this.selection_cols = selection_cols;
    }

    public HashMap<String, String> getKey_cols() {
        return key_cols;
    }

    public void setKey_cols(HashMap<String, String> key_cols) {
        this.key_cols = key_cols;
    }
}
