package com.scor.sgl.services.hcombine.domain;

import com.scor.sgl.services.hcombine.model.Options;
import com.univocity.parsers.csv.CsvParser;

import java.util.*;
import java.util.concurrent.ConcurrentMap;

public class ProcessChunkThread implements Runnable {

    private int id;
    private SyncWriter sw;
    private CsvParser otherparser;
    private int allSize;
    private int size;
    private final Options options;
    private List<String> header;
    private Map<String, Integer> columnsIndexes;
    private List<String> outputHeaders;
    private ConcurrentMap data;
    volatile int k = 0;

    public ProcessChunkThread(int id, SyncWriter sw, CsvParser otherparser, int allSize, int size, Options options, List<String> header, Map<String, Integer> columnsIndexes, List<String> outputHeaders, ConcurrentMap data) {
        this.id = id;
        this.sw = sw;
        this.otherparser = otherparser;
        this.allSize = allSize;
        this.size = size;
        this.options = options;
        this.header = header;
        this.columnsIndexes = columnsIndexes;
        this.outputHeaders = outputHeaders;
        this.data = data;
    }

    @Override
    public void run() {

        int padding = 0;
        if (options.getJoins()[id+1].getType().equals("RIGHT JOIN")) {
            padding=size;
        }

        int padding2 = 0;
        if (options.getJoins()[id+1].getType().equals("RIGHT JOIN")) {
            padding2=allSize;
        }


        String[] row;
        String key = "";
        while ((row = otherparser.parseNext()) != null) {
            List<String> keys = null;

            if (options.getJoins()[id+1].getType().equals("RIGHT JOIN")) {
                keys = new ArrayList<>(options.getJoins()[id+1].getKey_cols().values());
                for (int p=0; p<keys.size(); p++)
                    keys.set(p, "left."+keys.get(p));
            }
            else
                keys = new ArrayList<>(options.getJoins()[id+1].getKey_cols().keySet());

            for(String k: keys) {
              //  System.out.println(columnsIndexes.get(k)-allSize+padding2);
                try {
                    key += row[columnsIndexes.get(k) - allSize + padding2].trim().toLowerCase();
                } catch(Exception e) {
                    //System.out.println(columnsIndexes.get(k)-allSize+padding2);
                }
            }
            
            int step = 0;
            String[] current = new String[outputHeaders.size()];
            if (data.containsKey(key)) {
                String[] part = (String[]) data.get(key);
                for(int i=0; i< part.length; i++) {
                    if (outputHeaders.contains(header.get(i+padding2).replace("left.",""))) {
                        try {
                            current[step + size - padding] = part[i];
                            step++;
                        } catch(Exception e) {

                        }
                    }
                }
                data.remove(key);

                if (options.getJoins()[id+1].getType().equals("INNER JOIN")) {
                    step = 0;
                    for (int h = 0; h < row.length; h++) {
                        if (outputHeaders.contains(header.get(allSize - padding2 + h).replace("left.",""))) {
                            current[size - padding + step] = row[h];
                            step++;
                        }

                    }
                }

            }

            if (!options.getJoins()[id+1].getType().equals("INNER JOIN")) {
                step=0;
                for (int h = 0; h < row.length; h++) {
                    if (outputHeaders.contains(header.get(allSize - padding2 + h).replace("left.",""))) {
                        current[size - padding + step] = row[h];
                        step++;
                    }
                }
            }



            boolean empty = true;
            for (int i=0; i<current.length; i++) {
                if (current[i] != null) {
                    empty = false;
                    break;
                }
            }

            if (!empty)
                sw.write(current);
            key="";
        }

        String[] current = new String[outputHeaders.size()];
        if (options.getJoins()[id+1].getType().equals("FULL JOIN")) {
            for(Object k: data.keySet()) {
                String[] part = (String[]) data.get(k);
                int step = 0;
                for(int i=0; i< part.length; i++) {
                    if (outputHeaders.contains(header.get(i+padding2).replace("left.",""))) {
                        current[step + padding] = part[i];
                        step++;
                    }
                }
                boolean empty = true;
                for (int i=0; i<current.length; i++) {
                    if (current[i] != null) {
                        empty = false;
                        break;
                    }
                }

                if (!empty)
                    sw.write(current);
            }
        }

    }
}
