package com.scor.sgl.services.hcombine.model;

import javax.persistence.*;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.HashMap;

public class Options implements Serializable {

    @Column(length=10485760)
    private Source[] source;

    @Column(length=10485760)
    private Join[] joins;

    public Source[] getSource() {
        return source;
    }

    public void setSource(Source[] source) {
        this.source = source;
    }

    public Join[] getJoins() {
        return joins;
    }

    public void setJoins(Join[] joins) {
        this.joins = joins;
    }
}
