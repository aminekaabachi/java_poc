package com.scor.sgl.services.vcombine.dao;

import com.scor.sgl.services.vcombine.model.Job;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface JobRepository extends JpaRepository<Job, Long> {
  List<Job> findByStatus(String status);
}