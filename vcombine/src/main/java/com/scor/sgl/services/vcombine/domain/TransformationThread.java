package com.scor.sgl.services.vcombine.domain;

import com.scor.sgl.services.vcombine.dao.JobRepository;
import com.scor.sgl.services.vcombine.model.Job;
import com.scor.sgl.services.vcombine.model.Options;
import com.scor.sgl.services.vcombine.model.Source;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Component
@Scope("prototype")
public class TransformationThread implements Runnable {

    Job job;

    @Autowired
    JobRepository repository;

    private final List<Thread> workers;
    private SyncWriter sw;

    public TransformationThread(Job job) {
        this.job = job;
        this.workers = new ArrayList<>();
    }

    @Override
    public void run() {

        long start = System.currentTimeMillis();

        updateJobStatusRunning();

        CsvParser parser = getCsvParser();

        Options options = job.getOptions();
        parser.beginParsing(getReader(options.getSource()[0].getPath()));
        //File metadata =  new File(options.getSource()[0].getMetadata());
        HashMap<String,String> columnsTypes = null;
        HashMap<String,String> outputMetadata = new HashMap<>();
        List<String> header = Arrays.asList(parser.parseNext());
        Map<String, Integer> columnsIndexes = new HashMap<>();
        File file =new File(options.getSource()[0].getPath());
        String output = System.getProperty("user.home")+"/dummy/output/vcombine"+start+".csv";
        String outputMeta = options.getSource()[0].getMetadata();

        List<String> outputHeaders = header;
        sw = new SyncWriter(new File(output), outputHeaders);

        List<String[]> data = startWorkers(parser, options, header);
        int i;
        String[] row;
        //handling the rest data
        handlingEndData(options, header, data, data.size() > 0, 0, null);

        waitForWorkers();
        parser.stopParsing();

        for (int k = 1; k< options.getSource().length; k++) {
            combineWithNewFile(parser, options, header, data, k);
        }

        sw.close();

        updateJobStatusDone(start, output, outputMeta);

    }

    private void combineWithNewFile(CsvParser parser, Options options, List<String> header, List<String[]> data, int k) {
        int i;
        String[] row;
        System.out.println(options.getSource()[k].getPath());
        int[] positions = new int[header.size()];
        int pos = 0;
        HashMap<String, String> mapping = options.getMapping()[k];
        parser.beginParsing(getReader(options.getSource()[k].getPath()));
        List<String> header_current = Arrays.asList(parser.parseNext());

        for (String column: header){
            positions[pos] = header_current.indexOf(mapping.get(column));
            pos++;
        }

        i = 0;
        while ((row = parser.parseNext()) != null) {
            handlingEndData(options, header, data, data.size() >= 10000, 0, positions);

            if (i > 0 && i % 400000 == 0) {
                waitForWorkers();
            }

            data.add(row);
            i++;
        }
        //handling the rest data
        handlingEndData(options, header, data, data.size() > 0, k, positions);

        waitForWorkers();
        parser.stopParsing();
    }

    private void handlingEndData(Options options, List<String> header, List<String[]> data, boolean b, int i2, int[] o) {
        if (b) {
            Thread worker = new Thread(new ProcessChunkThread(i2, sw, options, header, o, data));
            worker.start();
            workers.add(worker);
            data.clear();
        }
    }

    @NotNull
    private List<String[]> startWorkers(CsvParser parser, Options options, List<String> header) {
        List<String[]> data = new ArrayList<>();
        String[] row = null;
        int i = 0;
        while ((row = parser.parseNext()) != null) {
            handlingEndData(options, header, data, data.size() >= 10000, 0, null);

            if (i > 0 && i % 400000 == 0) {
                waitForWorkers();
            }

            data.add(row);
            i++;
        }
        return data;
    }

    private void waitForWorkers() {
        for (Thread worker : workers) {
            try {
                worker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        workers.clear();
    }

    private void updateJobStatusDone(long start, String output, String outputMeta) {
        job.setStatus("done");
        Source outputSource = new Source();
        outputSource.setType("file");
        outputSource.setPath(output);
        outputSource.setMetadata(outputMeta);
        job.setOutput(outputSource);
        job.setTime((int) TimeUnit.MILLISECONDS.toSeconds((int) (System.currentTimeMillis() - start)));
        repository.save(job);
    }

    private void updateJobStatusRunning() {
        job.setStatus("running");
        repository.save(job);
    }

    @NotNull
    private CsvParser getCsvParser() {
        CsvParserSettings settings = new CsvParserSettings();
        settings.getFormat().setDelimiter(';');
        settings.getFormat().setLineSeparator("\n");
        return new CsvParser(settings);
    }

    public Reader getReader(String relativePath) {
        try {
            return new InputStreamReader(new FileInputStream(new File(relativePath)), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}