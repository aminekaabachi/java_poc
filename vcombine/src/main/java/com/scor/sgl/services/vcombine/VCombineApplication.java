package com.scor.sgl.services.vcombine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableDiscoveryClient
@EnableAsync
@EnableJpaRepositories
public class VCombineApplication {

	public static void main(String[] args) {
		SpringApplication.run(VCombineApplication.class, args);
	}

}
