package com.scor.sgl.services.vcombine.model;

import java.io.Serializable;
import java.util.HashMap;

public class Options implements Serializable {

    private Source[] source;
    private HashMap<String,String>[] mapping;

    public Source[] getSource() {
        return source;
    }

    public void setSource(Source[] source) {
        this.source = source;
    }

    public HashMap<String, String>[] getMapping() {
        return mapping;
    }

    public void setMapping(HashMap<String, String>[] mapping) {
        this.mapping = mapping;
    }
}
