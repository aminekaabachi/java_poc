package com.scor.sgl.services.vcombine.domain;

import com.scor.sgl.services.vcombine.model.Options;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ProcessChunkThread implements Runnable {

    private int id;
    private SyncWriter sw;
    private final Options options;
    private List<String> header;
    private int[] positions;
    volatile List<String[]> data;
    volatile int k = 0;

    public ProcessChunkThread(int id, SyncWriter sw, Options options, List<String> header, int[] positions, List<String[]> data) {
        this.id = id;
        this.sw = sw;
        this.options = options;
        this.header = header;
        this.positions = positions;
        this.data = new ArrayList<>(data);
    }

    @Override
    public void run() {
        ArrayList<Object[]> results = new ArrayList<>();

        System.out.println(data.size());

        if (id != 0) {
            for (String[] row : data) {
                String[] current_row = new String[header.size()];
                Arrays.fill(current_row, "");


                for (int p = 0; p < positions.length; p++) {
                    if (positions[p] != -1) {
                        current_row[p] = row[positions[p]];
                    }
                }

                results.add(current_row);
            }
        } else {
            for (String[] row : data) {
                results.add(row);
            }
        }

        System.out.println(id + " done "+ data.size());

        sw.writeAll(results);
        data.clear();
    }
}
