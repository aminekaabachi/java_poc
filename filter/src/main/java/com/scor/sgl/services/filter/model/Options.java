package com.scor.sgl.services.filter.model;

import java.io.Serializable;
import java.util.HashMap;

public class Options implements Serializable {

    private Source source;

    private String[] columns;

    private Filter filter;

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public String[] getColumns() {
        return columns;
    }

    public void setColumns(String[] columns) {
        this.columns = columns;
    }


    public Filter getFilter() {
        return filter;
    }

    public void setFilter(HashMap<String, Object> map) {
        this.filter = jsonToFilter(map);
    }


    public static Filter jsonToFilter(HashMap<String, Object> map) {
        String op = (String) map.get("op");
        if (!(op.equalsIgnoreCase("and") || op.equalsIgnoreCase("or"))) {
            return new LeafFilter(op, (String) map.get("left"), (String) map.get("right"));
        } else {
            return new CompositeFilter(op, jsonToFilter((HashMap<String, Object>) map.get("left")),
                    jsonToFilter((HashMap<String, Object>) map.get("right")));
        }
    }
}
