package com.scor.sgl.services.filter.domain;

import com.scor.sgl.services.filter.model.Options;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class ProcessChunkThread implements Runnable {

    private SyncWriter sw;
    private final Options options;
    private List<String> header;
    private List<String> outputHeaders;
    private HashMap<String, String> columnsTypes;
    volatile List<String[]> data;
    volatile int k = 0;

    public ProcessChunkThread(SyncWriter sw, Options options, List<String> header, List<String> outputHeaders, HashMap<String, String> columnsTypes, List<String[]> data) {
        this.sw = sw;
        this.options = options;
        this.header = header;
        this.outputHeaders = outputHeaders;
        this.columnsTypes = columnsTypes;
        this.data = new ArrayList<>(data);
    }

    public String type(String column) {
        return columnsTypes.getOrDefault(column, "string");
    }

    @Override
    public void run() {
        ArrayList<Object[]> results = new ArrayList<>();

        System.out.println(data.size());
        ConcurrentHashMap<String, String> rowValues = new ConcurrentHashMap<>();

        for (String[] row : data) {
            String[] returnedRow = new String[outputHeaders.size()];
            int k=0;


            for (int i = 0; i<header.size(); i++) {
                if (row[i] != null) {
                    rowValues.put(header.get(i), row[i]);
                }

                if(outputHeaders.contains(header.get(i))) {
                    returnedRow[k] = row[i];
                    k++;
                }
            }

            Boolean lineMatches = this.options.getFilter().evaluate(rowValues);
            rowValues.clear();

            if (lineMatches) {
                results.add(returnedRow);
            }


        }
        System.out.println("done "+ data.size());

        sw.writeAll(results);
        data.clear();
    }
}
