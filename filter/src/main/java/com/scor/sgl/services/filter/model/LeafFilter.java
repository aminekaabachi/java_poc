package com.scor.sgl.services.filter.model;

import java.util.concurrent.ConcurrentHashMap;

//@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@FilterID")
public class LeafFilter extends Filter {

    private String op;
    private String left;
    private String right;

    public LeafFilter(String op, String left, String right) {
        super();
        this.op = op;
        this.left = left;
        this.right = right;
    }

    public String getOp() {
        return op;
    }

    public void setOp(String op) {
        this.op = op;
    }

    public String getLeft() {
        return left;
    }

    public void setLeft(String left) {
        this.left = left;
    }

    public String getRight() {
        return right;
    }

    public void setRight(String right) {
        this.right = right;
    }

    @Override
    public Boolean evaluate(ConcurrentHashMap<String, String> rowValues) {

        String ll = "";

        if (rowValues.containsKey(this.left)) {
            if(this.right.contains("'"))
               ll = "'"+rowValues.get(this.left)+"'";
            else
                ll = rowValues.get(this.left);
        }
//            System.out.println(this.left + " " +  ll + " " + this.op + " " + this.right);

        switch (this.op) {
            case "=":
                if(this.right.contains("'"))
                    return ll.equals(this.right);
                else
                    return Double.parseDouble(ll) == Double.parseDouble(this.right);
            case "<>":
                if(this.right.contains("'"))
                    return !ll.equals(this.right);
                else
                    return Double.parseDouble(ll) != Double.parseDouble(this.right);
            case ">":
                if(this.right.contains("'"))
                    return ll.compareTo(this.right) > 0;
                else
                    return Double.parseDouble(ll) > Double.parseDouble(this.right);
            case ">=":
                if(this.right.contains("'"))
                    return ll.compareTo(this.right) >= 0;
                else
                    return Double.parseDouble(ll) >= Double.parseDouble(this.right);
            case "<":
                if(this.right.contains("'"))
                    return ll.compareTo(this.right) < 0;
                else
                    return Double.parseDouble(ll) < Double.parseDouble(this.right);
            case "<=":
                if(this.right.contains("'"))
                    return ll.compareTo(this.right) <= 0;
                else
                    return Double.parseDouble(ll) <= Double.parseDouble(this.right);
            case "like":
                if(this.right.contains("'"))
                    return this.like(ll, this.right);
                else
                    return Double.parseDouble(ll) <= Double.parseDouble(this.right);
            default: return false;
        }

    }

    public static boolean like(String str, String expr) {
        expr = expr.toLowerCase(); // ignoring locale for now
        expr = expr.replace(".", "\\."); // "\\" is escaped to "\" (thanks, Alan M)
        expr = expr.replace("?", ".");
        expr = expr.replace("%", ".*");
        str = str.toLowerCase();
        return str.matches(expr);
    }

}