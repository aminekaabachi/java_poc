package com.scor.sgl.services.filter.web;

import com.scor.sgl.services.filter.model.Job;
import com.scor.sgl.services.filter.model.Options;
import com.scor.sgl.services.filter.service.FilterTransformation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/")
@Api(value = "data", description = "Operations related to handling hcombine transformation jobs")
public class JobController {

    @Autowired
    FilterTransformation filterTransformation;

    @ApiOperation(value = "Gets list of all jobs", response = Iterable.class)
    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json")
    public List<Job> all(@RequestParam(defaultValue = "all") String status) {
        List<Job> all = filterTransformation.getAllJobs(status);
        return all;

    }

    @ApiOperation(value = "Starts new hcombine transformation job", response = Iterable.class)
    @RequestMapping(value = "/job/start", method = RequestMethod.POST, produces = "application/json")
    public Job start(@RequestBody Options options) {
        Job groupJob = filterTransformation.executeAsynchronously(options);
        return groupJob;

    }


    @ApiOperation(value = "Returns the status of a given job", response = Iterable.class)
    @RequestMapping(value = "/job/status", method = RequestMethod.GET, produces = "application/json")
    public Job status(@RequestParam long jobId) {
        Job groupJob = filterTransformation.getJobDetails(jobId);
        return groupJob;

    }

}
