package com.scor.sgl.services.filter.model;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class CompositeFilter extends Filter {

    private String op;
    private Filter left;
    private Filter right;

    public CompositeFilter(String op, Filter left, Filter right) {
        super();
        this.op = op;
        this.left = left;
        this.right = right;
    }

    public String getOp() {
        return op;
    }


    public void setOp(String op) {
        this.op = op;
    }

    public Filter getLeft() {
        return left;
    }

    public void setLeft(Filter left) {
        this.left = left;
    }

    public Filter getRight() {
        return right;
    }

    public void setRight(Filter right) {
        this.right = right;
    }

    @Override
    public Boolean evaluate(ConcurrentHashMap<String, String> rowValues) {
        try {
            //System.out.println("cf ");
            if (this.op.equals("and"))
                return this.left.evaluate(rowValues) && this.right.evaluate(rowValues);
            else if (this.op.equals("or"))
                return this.left.evaluate(rowValues) || this.right.evaluate(rowValues);
            else
                return false;
        } catch (Exception e) {
            return false;
        }
    }


    public static Filter jsonToFilter(HashMap<String, Object> map) {
        String op = (String) map.get("op");
        if (!(op.equalsIgnoreCase("and") || op.equalsIgnoreCase("or"))) {
            return new LeafFilter(op, (String) map.get("left"), (String) map.get("right"));
        } else {
            return new CompositeFilter(op, jsonToFilter((HashMap<String, Object>) map.get("left")),
                    jsonToFilter((HashMap<String, Object>) map.get("right")));
        }
    }
}