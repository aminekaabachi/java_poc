package com.scor.sgl.services.filter.model;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

public abstract class Filter implements Serializable {
    public abstract Boolean evaluate(ConcurrentHashMap<String, String> rowValues);
}