package com.scor.sgl.services.filter.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scor.sgl.services.filter.dao.JobRepository;
import com.scor.sgl.services.filter.model.Job;
import com.scor.sgl.services.filter.model.Options;
import com.scor.sgl.services.filter.model.Source;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@Scope("prototype")
public class TransformationThread implements Runnable {

    Job job;

    @Autowired
    JobRepository repository;

    private final List<Thread> workers;
    private SyncWriter sw;

    public TransformationThread(Job job) {
        this.job = job;
        this.workers = new ArrayList<>();
    }

    @Override
    public void run() {

        long start = System.currentTimeMillis();

        updateJobStatusRunning();

        CsvParser parser = getCsvParser();

        Options options = job.getOptions();
        HashMap<String, String> columnsTypes = getMetaTypes(parser, options);

        List<String> header = Arrays.asList(parser.parseNext());
        Map<String, Integer> columnsIndexes = new HashMap<>();
        HashMap<String,String> outputMetadata = new HashMap<>();

        for(String key: options.getColumns()) {
            columnsIndexes.put(key, header.indexOf(key));
            outputMetadata.put(key, columnsTypes.getOrDefault(key,"string"));
        }

        File file =new File(options.getSource().getPath());

        String output = System.getProperty("user.home")+"/dummy/output/filter"+start+".csv";
        String outputMeta = System.getProperty("user.home")+"/dummy/output/filter"+start+".json";

        try {
            (new ObjectMapper()).writeValue(new File(outputMeta),outputMetadata);
        } catch (IOException e) {
            e.printStackTrace();
        }

        List<String> outputHeaders = header.stream().filter(h-> outputMetadata.containsKey(h)).collect(Collectors.toList());
        sw = new SyncWriter(new File(output), outputHeaders);

        List<String[]> data = startWorkers(parser, options, columnsTypes, header, outputHeaders);
        handlingEndData(options, columnsTypes, header, outputHeaders, data, data.size() > 0);
        waitForWorkers();
        sw.close();

        updateJobStatusDone(start, output, outputMeta);

    }

    private void handlingEndData(Options options, HashMap<String, String> columnsTypes, List<String> header, List<String> outputHeaders, List<String[]> data, boolean b) {
        if (b) {
            Thread worker = new Thread(new ProcessChunkThread(sw, options, header, outputHeaders, columnsTypes, data));
            worker.start();
            workers.add(worker);
            data.clear();
        }
    }

    @NotNull
    private List<String[]> startWorkers(CsvParser parser, Options options, HashMap<String, String> columnsTypes, List<String> header, List<String> outputHeaders) {
        List<String[]> data = new ArrayList<>();
        String[] row = null;
        int i = 0;
        while ((row = parser.parseNext()) != null) {
            handlingEndData(options, columnsTypes, header, outputHeaders, data, data.size() >= 10000);

            if (i > 0 && i % 400000 == 0) {
                waitForWorkers();
            }

            data.add(row);
            i++;
        }
        return data;
    }

    private void waitForWorkers() {
        for (Thread worker : workers) {
            try {
                worker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        workers.clear();
    }

    private void updateJobStatusDone(long start, String output, String outputMeta) {
        job.setStatus("done");
        Source outputSource = new Source();
        outputSource.setType("file");
        outputSource.setPath(output);
        outputSource.setMetadata(outputMeta);
        job.setOutput(outputSource);
        job.setTime((int) TimeUnit.MILLISECONDS.toSeconds((int) (System.currentTimeMillis() - start)));
        repository.save(job);
    }

    @Nullable
    private HashMap<String, String> getMetaTypes(CsvParser parser, Options options) {
        parser.beginParsing(getReader(options.getSource().getPath()));
        File metadata =  new File(options.getSource().getMetadata());
        HashMap<String,String> columnsTypes = null;

        try {
            columnsTypes = new ObjectMapper().readValue(metadata, HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return columnsTypes;
    }

    @NotNull
    private CsvParser getCsvParser() {
        CsvParserSettings settings = new CsvParserSettings();
        settings.getFormat().setDelimiter(';');
        settings.getFormat().setLineSeparator("\n");
        return new CsvParser(settings);
    }

    private void updateJobStatusRunning() {
        job.setStatus("running");
        repository.save(job);
    }

    public Reader getReader(String relativePath) {
        try {
            return new InputStreamReader(new FileInputStream(new File(relativePath)), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}