package com.scor.sgl.services.hcombine;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scor.sgl.services.filter.model.CompositeFilter;
import com.scor.sgl.services.filter.model.Filter;
import com.scor.sgl.services.filter.model.LeafFilter;

import javax.script.ScriptException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;

public class FilterCompositeTest {
    public static void main(String [] args) throws IOException, ScriptException {
        LeafFilter lf1 = new LeafFilter("<", "20", "30");
        LeafFilter lf2 = new LeafFilter("<=", "'A'", "'B'");
        LeafFilter lf3 = new LeafFilter("like", "'AAA'", "'A??'");
        CompositeFilter cf1 = new CompositeFilter("and", lf1, lf2);
        CompositeFilter cf2 = new CompositeFilter("and", cf1, lf3);
        HashMap<String, Object> bigMap = new ObjectMapper().readValue(new File("/home/g0del/dummy/hcombine.json"), HashMap.class);
        Filter filter = jsonToFilter((HashMap<String, Object>) bigMap.get("hcombine"));
        //System.out.println(cf2.evaluate());

      /*  ScriptEngineManager mgr = new ScriptEngineManager();
        ScriptEngine engine = mgr.getEngineByName("JavaScript");
        System.out.println(engine.eval(hcombine.evaluate()));*/

    }

    public static Filter jsonToFilter(HashMap<String, Object> map) {
        String op = (String) map.get("op");
        if (!(op.equalsIgnoreCase("and") || op.equalsIgnoreCase("or"))) {
            return new LeafFilter(op, (String) map.get("left"), (String) map.get("right"));
        } else {
            return new CompositeFilter(op, jsonToFilter((HashMap<String, Object>) map.get("left")),
                    jsonToFilter((HashMap<String, Object>) map.get("right")));
        }
    }
}
