package com.scor.hda.services.gateway.dao;

import com.scor.hda.services.gateway.model.Flow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface FlowRepository extends JpaRepository<Flow, Long> {
  List<Flow> findByStatus(String status);
}