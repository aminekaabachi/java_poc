package com.scor.hda.services.gateway.model;

import com.fasterxml.jackson.databind.JsonNode;

import java.io.Serializable;

public class FilterJob implements Serializable {


   private String status;
   private Long id;
   private int time;
   private Source output;

    public Source getOutput() {
        return output;
    }

    public void setOutput(Source output) {
        this.output = output;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }
}
