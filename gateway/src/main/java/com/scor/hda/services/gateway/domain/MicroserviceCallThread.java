package com.scor.hda.services.gateway.domain;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.scor.hda.services.gateway.dao.FlowRepository;
import com.scor.hda.services.gateway.model.FilterJob;
import com.scor.hda.services.gateway.model.Flow;
import com.scor.hda.services.gateway.model.Node;
import com.scor.hda.services.gateway.model.Source;
import com.scor.hda.services.gateway.service.FilterMicroservice;
import com.scor.hda.services.gateway.service.Microservice;

import java.util.List;
import java.util.concurrent.ConcurrentMap;

public class MicroserviceCallThread implements Runnable {


    private final int k;
    private final Node current;
    private final Flow job;
    private final FlowRepository repository;
    private FilterMicroservice microservice;
    private final ConcurrentMap<Integer, Source> output;

    public MicroserviceCallThread(int k, Node current, Flow job, FlowRepository repository, Microservice microservice, ConcurrentMap<Integer, Source> output) {
        this.k = k;
        this.current = current;
        this.job = job;
        this.repository = repository;
        this.microservice = (FilterMicroservice) microservice;
        this.output = output;
    }


    @Override
    public void run() {


        if (!current.getParameters().toString().equals("{}")) {
            Source input = null;
            int h = 1;

            for (List<Integer> adj : job.getAdjacency().values()) {
                System.out.print(adj.toArray());
                if (adj.contains(k)) {
                    input = output.get(h);
                }
                h++;
            }

            ObjectNode autoParameters = (ObjectNode) current.getParameters();
            autoParameters.set("source",  new ObjectMapper().convertValue(input, JsonNode.class));
            FilterJob fj = microservice.startJob(autoParameters);

            while(!fj.getStatus().equals("done")) {
                fj = microservice.jobStatus(fj.getId());
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            job.setWorkTime(job.getWorkTime()+fj.getTime());
            repository.save(job);
            output.put(k, fj.getOutput());
        }
    }

}
