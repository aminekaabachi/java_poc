package com.scor.hda.services.gateway.web;

import com.scor.hda.services.gateway.model.Flow;
import com.scor.hda.services.gateway.model.Graph;
import com.scor.hda.services.gateway.service.FlowService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/")
@Api(value = "data", description = "Operations related to handling hcombine transformation jobs")
public class FlowController {

    @Autowired
    FlowService flowService;

    @ApiOperation(value = "Gets list of all transformation flow jobs", response = Iterable.class)
    @RequestMapping(value = "/all", method = RequestMethod.GET, produces = "application/json")
    public HashMap<String, Object> all(@RequestParam(defaultValue = "all") String status) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("all",flowService.getAllJobs(status));
        return result;

    }

    @ApiOperation(value = "Starts new transformation flow job", response = Iterable.class)
    @RequestMapping(value = "/flow/start", method = RequestMethod.POST, produces = "application/json")
    public HashMap<String, Object> start(@RequestBody Flow flow) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("details", flowService.executeAsynchronously(flow));
        return result;
    }


/*
    @ApiOperation(value = "Starts new simple flow job", response = Iterable.class)
    @RequestMapping(value = "/flow/simple", method = RequestMethod.GET, produces = "application/json")
    public HashMap<String, Object> simple(@RequestParam(defaultValue = "100K") String size) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("details", flowService.executeSimple(size));
        return result;
    }



    @ApiOperation(value = "Starts new medium flow job", response = Iterable.class)
    @RequestMapping(value = "/flow/medium", method = RequestMethod.GET, produces = "application/json")
    public HashMap<String, Object> medium(@RequestParam(defaultValue = "100K") String size) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("details", flowService.executeMedium(size));
        return result;
    }
*/


    @ApiOperation(value = "Returns the status of a given flow job", response = Iterable.class)
    @RequestMapping(value = "/job/status", method = RequestMethod.GET, produces = "application/json")
    public HashMap<String, Object> status(@RequestParam long jobId) {
        HashMap<String, Object> result = new HashMap<>();
        result.put("details", flowService.getJobDetails(jobId));
        return result;
    }

}
