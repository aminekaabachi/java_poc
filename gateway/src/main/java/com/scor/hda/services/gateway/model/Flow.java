package com.scor.hda.services.gateway.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

@Entity
public class Flow implements Serializable {



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String status = "pending";
    private long totalTime = 0;
    private long workTime = 0;
    private Source output;
    private LinkedList<Integer> detailedTiming = new LinkedList<>();

    public LinkedList<Integer> getDetailedTiming() {
        return detailedTiming;
    }

    public void setDetailedTiming(LinkedList<Integer> detailedTiming) {
        this.detailedTiming = detailedTiming;
    }

    public void addTiming(int time) {
        this.detailedTiming.add(time);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Transient
    private HashMap<Integer, ArrayList<Integer>> adjacency;

    @Transient
    private HashMap<Integer, Node> nodes;

    public HashMap<Integer, ArrayList<Integer>> getAdjacency() {
        return adjacency;
    }


    public Source getOutput() {
        return output;
    }

    public void setOutput(Source output) {
        this.output = output;
    }

    public void setAdjacency(HashMap<Integer, ArrayList<Integer>> adjacency) {
        this.adjacency = adjacency;
    }

    public HashMap<Integer, Node> getNodes() {
        return nodes;
    }

    public void setNodes(HashMap<Integer, Node> nodes) {
        this.nodes = nodes;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }

    public long getWorkTime() {
        return workTime;
    }

    public void setWorkTime(long workTime) {
        this.workTime = workTime;
    }
}
