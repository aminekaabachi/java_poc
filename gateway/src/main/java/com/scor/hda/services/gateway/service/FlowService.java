package com.scor.hda.services.gateway.service;


import com.scor.hda.services.gateway.dao.FlowRepository;
import com.scor.hda.services.gateway.domain.JobThread;
import com.scor.hda.services.gateway.domain.MediumJobThread;
import com.scor.hda.services.gateway.domain.SimpleJobThread;
import com.scor.hda.services.gateway.model.Flow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Scope("prototype")
public class FlowService {

    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    FlowRepository repository;

    public Flow executeAsynchronously(Flow flow) {
            repository.save(flow);
            JobThread job = applicationContext.getBean(JobThread.class, flow);
            taskExecutor.execute(job);
            return flow;
    }


    public List<Flow> getAllJobs(String status) {
        List<Flow> allJobs;
        if (status.equals("all"))
            allJobs = repository.findAll();
        else
            allJobs = repository.findByStatus(status);
        return allJobs;
    }

    public Flow getJobDetails(long jobId) {
        return repository.findOne(jobId);
    }
/*
    public Object executeSimple(String size) {
        Flow n = repository.save(new Flow());
        SimpleJobThread job = applicationContext.getBean(SimpleJobThread.class, n, size);
        taskExecutor.execute(job);
        return n;
    }

    public Object executeMedium(String size) {
        Flow n = repository.save(new Flow());
        MediumJobThread job = applicationContext.getBean(MediumJobThread.class, n, size);
        taskExecutor.execute(job);
        return n;
    }
*/
}
