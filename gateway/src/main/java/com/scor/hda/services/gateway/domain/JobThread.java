package com.scor.hda.services.gateway.domain;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scor.hda.services.gateway.dao.FlowRepository;
import com.scor.hda.services.gateway.model.Flow;

import com.scor.hda.services.gateway.model.Graph;
import com.scor.hda.services.gateway.model.Node;
import com.scor.hda.services.gateway.model.Source;
import com.scor.hda.services.gateway.service.FilterMicroservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@Scope("prototype")
public class JobThread implements Runnable {

    Flow job;
    private final List<Thread> workers;

    @Autowired
    FlowRepository repository;

    @Autowired
    FilterMicroservice filter;

    public JobThread(Flow job) {
        this.job = job;
        workers = new ArrayList<>();
    }

    @Override
    public void run() {

        long start = System.currentTimeMillis();

        job.setStatus("running");
        repository.save(job);

        Graph flowGraph = new Graph(job.getAdjacency());
        ArrayList<ArrayList<Integer>> layers = flowGraph.breadthFirstSearchLayers(1);
        flowGraph.printBfsLayers(layers);

        ConcurrentMap<Integer, Source> output = new ConcurrentHashMap<>();

        JsonNode parameters = job.getNodes().get(1).getParameters();
        Source cc = new Source();
        cc.setType("file");
        cc.setPath( parameters.get("source").get("path").toString().replaceAll("\"", ""));
        cc.setMetadata(parameters.get("source").get("metadata").toString().replaceAll("\"", ""));
        output.put(1, cc);

        int currentLayer = 1;
        while (currentLayer <= layers.size()-1) {
            for (int k : layers.get(currentLayer)) {
                Node current = job.getNodes().get(k);
                Thread worker = new Thread(new MicroserviceCallThread(k, current, job, repository, filter, output));
                worker.start();
                workers.add(worker);
            }

            for (Thread worker : workers) {
                try {
                    worker.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            workers.clear();
            currentLayer++;
        }

        job.setStatus("done");
        int last = flowGraph.getNodeCount()-2;
        job.setOutput(output.get(last));
        job.setTotalTime((int) TimeUnit.MILLISECONDS.toSeconds((int) (System.currentTimeMillis() - start)));
        repository.save(job);
    }

}