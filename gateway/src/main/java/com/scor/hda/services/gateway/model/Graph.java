package com.scor.hda.services.gateway.model;
import java.util.*;

public class Graph {
    private ArrayList<LinkedList<Integer>> graphAdjacencyList = new ArrayList<LinkedList<Integer>>();
    private Integer[] validNodes = null;

    public Graph(HashMap<Integer, ArrayList<Integer>> adjacency){
        this.validNodes = new Integer[adjacency.size()];

        int i=0;
        for (int key: adjacency.keySet()) {
            validNodes[i] = key;
            i++;
        }

        graphAdjacencyList.add( new LinkedList<Integer>() );
        for (List<Integer> next : adjacency.values())
            graphAdjacencyList.add( new LinkedList<Integer>( next ) );

    }

    public ArrayList<LinkedList<Integer>> getAdjacencyList(){
        return graphAdjacencyList;
    }

    public void setAdjacencyList(ArrayList<LinkedList<Integer>> newGraphStructure){
        graphAdjacencyList = newGraphStructure;
    }

    public int getNodeCount(){
        return getAdjacencyList().size();
    }

    public boolean isValidNode(int node){
        return Arrays.asList(validNodes).contains(node);
    }



    public ArrayList<ArrayList<Integer>> breadthFirstSearchLayers(int startingNode){
        boolean[] visitedNodes = new boolean[ getNodeCount() +1];

        int layerCounter = 0;
        ArrayList<ArrayList<Integer>> layers = new ArrayList<ArrayList<Integer>>();
        layers.add(new ArrayList<Integer>());
        layers.get(0).add(startingNode);
        visitedNodes[startingNode]=true;

        while( !layers.get(layerCounter).isEmpty()){
            layers.add(new ArrayList<Integer>());

            for(int u : layers.get(layerCounter)){
                for(int v : getAdjacencyList().get(u)){
                    if(!visitedNodes[v]){
                        int k = 0;
                        Boolean all = true;

                        for (List nds: getAdjacencyList()) {
                            if (nds.contains(v) && !visitedNodes[k]) {
                                all = false;
                            }
                            k++;
                        }

                        if (all) {
                            Boolean sorry = false;
                            for(int h : layers.get(layerCounter + 1) ) {
                                if (getAdjacencyList().get(h).contains(v)) {
                                    sorry = true;
                                }
                            }
                            if (!sorry) {
                                //System.out.println("adding "+ v +" to "+(layerCounter+1));
                                visitedNodes[v] = true;
                                layers.get(layerCounter + 1).add(v);
                            }
                        }
                    }
                }
            }
            layerCounter++;
        }

        return layers;
    }

    public void printBfsLayers(ArrayList<ArrayList<Integer>> layers){
        for(int i=0; i<(layers.size()-1); i++){
            System.out.print("Layer "+i+":  ");
            for(int vertex : layers.get(i)){
                System.out.print(vertex+ "  ");
            }
            System.out.println();
        }
    }

    @Override
    public String toString() {
        String adjacecyListAsLines="";
        for(int i=1; i< getNodeCount(); i++){
            adjacecyListAsLines += "{"+i+"}  ";
            adjacecyListAsLines += getAdjacencyList().get(i)+"\n";
        }
        return adjacecyListAsLines;
    }

}