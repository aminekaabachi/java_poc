package com.scor.hda.services.gateway.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.scor.hda.services.gateway.model.FilterJob;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient("vcombine")
public interface VCombineMicroservice extends Microservice {
    @RequestLine("GET /all")
    @RequestMapping(value = "/all", method = RequestMethod.GET)
    List<FilterJob> all();

    @RequestLine("POST /job/start")
    @RequestMapping(value = "/job/start", method = RequestMethod.POST)
    FilterJob startJob(JsonNode options);

    @RequestLine("GET /job/status")
    @GetMapping(value = "/job/status")
    FilterJob jobStatus(@RequestParam("jobId") long jobId);
}

