package com.scor.sgl.services.group.service;

import com.scor.sgl.services.group.dao.JobRepository;
import com.scor.sgl.services.group.model.Job;
import com.scor.sgl.services.group.model.Options;
import com.scor.sgl.services.group.domain.TransformationThread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Scope("prototype")
public class GroupTransformation {

    @Autowired
    private TaskExecutor taskExecutor;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    JobRepository repository;

    public Job executeAsynchronously(Options options) {
            Job groupJob =  new Job();
            groupJob.setOptions(options);
            repository.save(groupJob);

            TransformationThread job = applicationContext.getBean(TransformationThread.class, groupJob);
            taskExecutor.execute(job);

            return groupJob;
    }

    public List<Job> getAllJobs(String status) {
        List<Job> allJobs;
        if (status.equals("all"))
            allJobs = repository.findAll();
        else
            allJobs = repository.findByStatus(status);
        return allJobs;
    }

    public Job getJobDetails(long jobId) {
        return repository.findOne(jobId);
    }
}
