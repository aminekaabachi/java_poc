package com.scor.sgl.services.group.model;

import java.io.Serializable;

public class Options implements Serializable {
    private Source source;

    private String[] keys;

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public String[] getKeys() {
        return keys;
    }

    public void setKeys(String[] keys) {
        this.keys = keys;
    }

    public GroupBy[] getGroup_by() {
        return group_by;
    }

    public void setGroup_by(GroupBy[] group_by) {
        this.group_by = group_by;
    }

    private GroupBy[] group_by;

}
