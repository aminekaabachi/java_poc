package com.scor.sgl.services.group.model;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.Serializable;

public class Source implements Serializable {

    private String type;
    private String path;
    private String metadata;

    public Source() {
    }

    public Source(String data) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        Source source = mapper.readValue(data, Source.class);
        this.type = source.type;
        this.path =source.path;
        this.metadata = source.metadata;
    }

    public String getMetadata() {
        return metadata;
    }

    public void setMetadata(String metadata) {
        this.metadata = metadata;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
