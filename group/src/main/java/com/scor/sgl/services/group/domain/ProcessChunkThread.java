package com.scor.sgl.services.group.domain;

import com.scor.sgl.services.group.model.GroupBy;
import com.scor.sgl.services.group.model.Options;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProcessChunkThread implements Runnable {

    private SyncWriter sw;
    private final Options options;
    private List<String> header;
    private List<String> outputHeaders;
    private HashMap<String, String> columnsTypes;
    volatile List<String[]> data;
    volatile int k = 0;

    public ProcessChunkThread(SyncWriter sw, Options options, List<String> header, List<String> outputHeaders, HashMap<String, String> columnsTypes, List<String[]> data) {
        this.sw = sw;
        this.options = options;
        this.header = header;
        this.outputHeaders = outputHeaders;
        this.columnsTypes = columnsTypes;
        this.data = new ArrayList<>(data);
    }

    public String type(String column) {
        return columnsTypes.getOrDefault(column, "string");
    }

    @Override
    public void run() {
        String[] transform = new String[header.size()];

        int i = 0;
        for (String column : header) {
            boolean group_by = false;
            for (GroupBy gb : options.getGroup_by()) {
                if (column.equals(gb.getColumn()) && i<header.size()) {
                    transform[i] = gb.getOperation();
                    group_by = true;
                    i++;
                    break;
                }
            }
            for (String key : options.getKeys()) {
                if (column.equals(key) && i<header.size()) {
                    group_by = true;
                    transform[i] = "first";
                    i++;
                    break;
                }
            }
            if (!group_by && i<header.size()) {
                transform[i] = "no";
                i++;
            }
        }

        String[] result = new String[outputHeaders.size()];

        int rowId = 0;
        for (String[] row : data) {
            try {
            k = 0;
            for (int j = 0; j < row.length; j++) {

                /* common for all types */

                //first && last
                if (transform[j].equals("first")) {
                    if (rowId == 0)
                        result[k] = row[j];
                    k++;
                }
                if (transform[j].equals("last")) {
                    if (rowId == data.size() - 1)
                        result[k] = row[j];
                    k++;
                }
                //count
                if (transform[j].equals("count")) {
                    if (rowId == 0)
                        result[k] = String.valueOf(data.size());
                    k++;
                }

                /*end common for all types*/

                DecimalFormat df = new DecimalFormat("0.#");

                //min
                if (transform[j].equals("min")) {
                    if (rowId == 0)
                                result[k] = row[j];
                    else if (rowId > 0)
                        switch(type(this.header.get(j))) {
                            case "numeric":
                                if (Double.parseDouble(row[j]) < Double.parseDouble(result[k]))
                                    result[k] =  df.format(Double.parseDouble(row[j]));
                                break;
                            case "date":
                                break;
                            default:
                                if (row[j].compareTo(result[k]) < 0)
                                    result[k] = row[j];
                                break;
                        }
                    k++;
                }

                //max
                if (transform[j].equals("max")) {
                    if (rowId == 0)
                        result[k] = row[j];
                    else if (rowId > 0)
                        switch(type(this.header.get(j))) {
                            case "numeric":
                                if (Double.parseDouble(row[j]) > Double.parseDouble(result[k]))
                                    result[k] =  df.format(Double.parseDouble(row[j]));
                                break;
                            case "date":
                                break;
                            default:
                                if (row[j].compareTo(result[k]) > 0)
                                    result[k] = row[j];
                                break;
                        }
                    k++;
                }

                //sum
                if (transform[j].equals("sum")) {
                    if (rowId == 0)
                        result[k] = row[j];
                    else if (rowId > 0)
                        result[k] = df.format(Double.parseDouble(result[k]) + Double.parseDouble(row[j]));
                    k++;
                }

                //average
                if (transform[j].equals("average")) {
                    if (rowId == 0)
                        result[k] =  df.format(Double.parseDouble(row[j])/data.size());
                    else if (rowId > 0)
                        result[k] = df.format(Double.parseDouble(result[k]) + Double.parseDouble(row[j])/data.size());
                    k++;
                }

                //concat
                if (transform[j].equals("concat")) {
                    if (rowId == 0)
                        result[k] = row[j].trim();
                    else if (rowId > 0)
                        result[k] += ", " + row[j].trim();
                    k++;
                }
            } }
            catch (Exception e) {

            }
            rowId++;
            //System.out.println(this.k);
        }

        sw.write(result);
        data.clear();
    }
}
