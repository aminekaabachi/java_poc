package com.scor.sgl.services.group.model;

import java.io.Serializable;

public class GroupBy implements Serializable{

    private String column;
    private String operation;

    public String getColumn() {
        return column;
    }

    public void setColumn(String column) {
        this.column = column;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }


}
