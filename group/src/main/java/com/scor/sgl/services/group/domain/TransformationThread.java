package com.scor.sgl.services.group.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.scor.sgl.services.group.dao.JobRepository;
import com.scor.sgl.services.group.model.GroupBy;
import com.scor.sgl.services.group.model.Job;
import com.scor.sgl.services.group.model.Options;
import com.scor.sgl.services.group.model.Source;
import com.univocity.parsers.csv.CsvParser;
import com.univocity.parsers.csv.CsvParserSettings;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@Scope("prototype")
public class TransformationThread implements Runnable {

    Job job;

    @Autowired
    JobRepository repository;

    private final List<Thread> workers;
    private SyncWriter sw;

    public TransformationThread(Job job) {
        this.job = job;
        this.workers = new ArrayList<>();
    }

    @Override
    public void run() {

        long start = System.currentTimeMillis();

        updateJobStatusRunnig();

        CsvParser parser = getCsvParser();

        Options options = job.getOptions();
        parser.beginParsing(getReader(options.getSource().getPath()));
        File metadata =  new File(options.getSource().getMetadata());
        HashMap<String, String> columnsTypes = getMetaTypes(metadata);
        HashMap<String,String> outputMetadata = new HashMap<>();

        //Indexing keys and group_by by columns
        List<String> header = Arrays.asList(parser.parseNext());
        Map<String, Integer> columnsIndexes = new HashMap<>();

        SetMetadata(options, columnsTypes, outputMetadata, header, columnsIndexes);

        File file =new File(options.getSource().getPath());

        double bytes = file.length();
        double kilobytes = (bytes / 1024);
        double megabytes = (kilobytes / 1024);
        double gigabytes = (megabytes / 1024);
        String path = System.getProperty("user.home")+"/dummy/group"+start+".db";
        String output = System.getProperty("user.home")+"/dummy/output/group"+start+".csv";

        String outputMeta = System.getProperty("user.home")+"/dummy/output/group"+start+".json";

        try {
            (new ObjectMapper()).writeValue(new File(outputMeta),outputMetadata);
        } catch (IOException e) {
            e.printStackTrace();
        }

        DB db = null;
        if (gigabytes < 5) {
            db = DBMaker.memoryDB().make();
        } else {
            db = DBMaker
                    .fileDB(path)
                    .fileMmapEnable()
                    .closeOnJvmShutdown()
                    .make();
        }
        ConcurrentMap map = db.hashMap("mapped").create();
        String[] row = null;
        String key = "";

        buildingKeysMap(parser, options, columnsIndexes, map, key);

        List<String> outputHeaders = header.stream().filter(h-> outputMetadata.containsKey(h)).collect(Collectors.toList());

        sw = new SyncWriter(new File(output), outputHeaders);
        startWorkers(options, columnsTypes, header, map, outputHeaders);
        waitForWorkers();
        sw.close();
        map.clear();
        db.close();
        updateJobStatusDone(start, output, outputMeta);

    }

    private void updateJobStatusDone(long start, String output, String outputMeta) {
        job.setStatus("done");
        Source outputSource = new Source();
        outputSource.setType("file");
        outputSource.setPath(output);
        outputSource.setMetadata(outputMeta);
        job.setOutput(outputSource);
        job.setTime((int) TimeUnit.MILLISECONDS.toSeconds((int) (System.currentTimeMillis() - start)));
        repository.save(job);
    }

    private void waitForWorkers() {
        for (Thread worker : workers) {
            try {
                worker.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        workers.clear();
    }

    private void startWorkers(Options options, HashMap<String, String> columnsTypes, List<String> header, ConcurrentMap map, List<String> outputHeaders) {
        for (Object k : map.keySet()) {
            List<String[]> data = (List<String[]>) map.get(k);
            Thread worker = new Thread(new ProcessChunkThread(sw, options, header, outputHeaders, columnsTypes, data));
            worker.start();
            workers.add(worker);
        }
    }

    private void buildingKeysMap(CsvParser parser, Options options, Map<String, Integer> columnsIndexes, ConcurrentMap map, String key) {
        String[] row;
        while ((row = parser.parseNext()) != null) {
                for(String k: options.getKeys()) {
                    try {
                        key += row[columnsIndexes.get(k)].trim().toLowerCase();
                } catch (Exception e) {
                    //TODO: handle exceptions in next version
                }
                }
                List<String[]> rows = (List<String[]>) map.getOrDefault(key,new ArrayList<>());
                rows.add(row);
                map.put(key, rows);
                key = "";

        }
    }

    private void SetMetadata(Options options, HashMap<String, String> columnsTypes, HashMap<String, String> outputMetadata, List<String> header, Map<String, Integer> columnsIndexes) {
        for(String key: options.getKeys()) {
            columnsIndexes.put(key, header.indexOf(key));
            outputMetadata.put(key, columnsTypes.getOrDefault(key,"string"));
        }

        for(GroupBy gb: options.getGroup_by()) {
            outputMetadata.put(gb.getColumn(), columnsTypes.getOrDefault(gb.getColumn(),"string"));
        }
    }

    @Nullable
    private HashMap<String, String> getMetaTypes(File metadata) {
        HashMap<String,String> columnsTypes = null;

        try {
            columnsTypes = new ObjectMapper().readValue(metadata, HashMap.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return columnsTypes;
    }

    @NotNull
    private CsvParser getCsvParser() {
        CsvParserSettings settings = new CsvParserSettings();
        settings.getFormat().setDelimiter(';');
        settings.getFormat().setLineSeparator("\n");
        return new CsvParser(settings);
    }

    private void updateJobStatusRunnig() {
        job.setStatus("running");
        repository.save(job);
    }

    public Reader getReader(String relativePath) {
        try {
            return new InputStreamReader(new FileInputStream(new File(relativePath)), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

}