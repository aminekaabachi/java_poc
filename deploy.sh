mkdir ~/elasticsearch
mkdir ~/elasticsearch/data

export M2_HOME=/opt/maven
export PATH=${M2_HOME}/bin:${PATH}
mvn package

docker-compose -f docker-compose.yml -f docker-compose.dev.yml build
docker-compose up
